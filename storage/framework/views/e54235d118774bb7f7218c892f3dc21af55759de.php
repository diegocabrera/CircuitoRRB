<?php $__env->startSection('content'); ?>
    <div style="height:80px;"></div>
<div class="col-md-12 col-sm-10 col-xs-10 col-lg-12">
    <?php $i=1; ?>
    <?php $__currentLoopData = $audios; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $audio): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($i == 1 ): ?>
        <?php endif; ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class=" titulo-noticia"><?php echo $audio->titulo; ?></h3>
            <div class="row">
                <div class="noticias card">
                    <div class="row">
                        <div class="col-md-12 col-sm-10 col-xs-10 col-lg-12 noti-top"></div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="" style="margin-top:1em;">
                            <center>
                                <audio style="width:80%;"  src="<?php echo e(url('public/audio/'. $audio->archivo)); ?>" controls="controls" preload="">

                                </audio>
                            </center>
                        </div>
                    </div>
                    <div class="texto-resumen" style="text-align:justify">
                        <p style="margin-left:5px;margin-right:5px;"><?php echo $audio->descripcion; ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('pagina::layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>