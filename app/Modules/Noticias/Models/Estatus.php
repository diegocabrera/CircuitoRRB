<?php
namespace App\Modules\Noticias\Models;

use App\Modules\Base\Models\Modelo;

class Estatus extends Modelo
{
	protected $table = 'estatus';
    protected $fillable = ["nombre"];
}
