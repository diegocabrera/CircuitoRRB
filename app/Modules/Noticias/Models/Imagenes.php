<?php

namespace App\Modules\Noticias\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Noticias\Models\Noticias;

class imagenes extends Modelo
{
    protected $table = 'imagenes';
    protected $fillable = ['noticias_id', 'archivo', 'tamano', 'descripcion', 'leyenda'];

    public function noticias(){
      // belongsTo = "pertenece a" | hace relacion desde el detalle hasta el maestro
      return $this->belongsTo('App\Modules\Noticias\Models\Noticias');
    }
}
