@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Programas']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Programas.',
        'columnas' => [
            'Titulo' => '100'
        ]
    ])
@endsection

@section('content')
    {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
       <div class="row">
            <div class="profile-sidebar col-md-3" style="margin-bottom: 35px;">
				<div class="portlet light profile-sidebar-portlet ">
					<div class="mt-element-overlay">
						<div class="row">
							<div class="col-md-12">
                                <div class="mt-overlay-6">
                                    <img  id="foto" src="{{ url('public/img/usuarios/user.png') }}" class="img-responsive" alt="">
                                    <div class="mt-overlay">
                                        <h2> </h2>
                                        <p>
                                            <input id="upload" name="foto" type="file" />
                                            <a href="#" id="upload_link" class="mt-info uppercase btn default btn-outline">
                                                <i class="fa fa-camera"></i>
                                            </a>
                                        </p>
                                    </div>
                                    <h4 style="color:#fff;font-weight:bold;">Imagen de perfil</h4>
                                </div>
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
            <div class="col-md-9">
                <div class="panel panel-info ">
                    <div class="panel-heading">
                        <center>
                            <h3 class="panel-title">Programas</h3>
                        </center>
                    </div>
                    <div class="panel-body">
                        {{ Form::bsText('titulo', '', [
                            'label' => 'Nombre del Programa',
                            'placeholder' => 'Nombre del Programa',
                            'required' => 'required',
                            'class_cont' => 'col-md-6'
                        ]) }}
                        {{ Form::bsText('locutor', '', [
                            'label' => 'Nombre del locutor',
                            'placeholder' => 'Nombre del locutor',
                            'required' => 'required',
                             'class_cont' => 'col-md-6'
                        ]) }}

                        {{ Form::bsText('hora_ini', '', [
                            'label' => 'Hora Inicio del programa',
                            'placeholder' => 'Hora Inicio del Programa',
                            'required' => 'required',
                            'class_cont' => 'col-md-4'
                        ]) }}

                        {{ Form::bsText('hora_fin', '', [
                            'label' => 'Hora Final del programa',
                            'placeholder' => 'Hora Final del Programa',
                            'required' => 'required',
                             'class_cont' => 'col-md-4'
                        ]) }}

                        {{ Form::bsSelect('dias',$controller->dias(), '', [
                            'label' => 'Dias',
                            'class' => 'bs-select',
                            'multiple' => 'multiple',
                            'name'    => 'dias[]'
                        ]) }}

                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection
