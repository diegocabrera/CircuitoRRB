<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Programas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('programas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 200);
            $table->string('dias', 200);
            $table->time('hora_ini')->nulleable();
            $table->time('hora_fin')->nulleable();
            $table->string('locutor', 100);
            $table->string('url', 255);

            $table->timestamps();
            $table->softDeletes();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

          Schema::dropIfExists('programas');
    }
}
