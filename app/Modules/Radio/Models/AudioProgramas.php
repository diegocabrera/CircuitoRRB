<?php

namespace App\Modules\Radio\Models;

use App\Modules\base\Models\Modelo;

use App\Modules\Radio\Models\Programas;

class AudioProgramas extends modelo
{
    protected $table = 'audio_programas';
    protected $fillable = ["titulo","url","programas_id"];
    protected $campos = [
    'titulo' => [
        'type' => 'text',
        'label' => 'Titulo',
        'placeholder' => 'Titulo del Audio Programas'
    ],
    'url' => [
        'type' => 'text',
        'label' => 'Url',
        'placeholder' => 'Url del Audio Programas'
    ],
    'programas_id' => [
        'type' => 'select',
        'label' => 'Programas',
        'placeholder' => '- Seleccione un Programas',
     
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['programas_id']['options'] = Programas::pluck('titulo', 'id');
    }

    public function programas()
    {
        return $this->belongsTo('App\ModulesRadio\Models\Programas');
    }

    
}