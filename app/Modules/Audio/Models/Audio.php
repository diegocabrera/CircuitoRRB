<?php

namespace App\Modules\Audio\Models;

use App\Modules\Base\Models\Modelo;

class Audio extends Modelo
{
    protected $table = 'audio';
    protected $fillable = ["categorias_id","archivo","titulo","slug","descripcion","activo"];

    protected $hidden = ["created_at","updated_at","deleted_at"];


}
