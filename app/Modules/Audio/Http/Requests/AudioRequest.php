<?php

namespace App\Modules\Audio\Http\Requests;

use App\Http\Requests\Request;

class AudioRequest extends Request {
    protected $reglasArr = [
		'categorias_id' => ['required', 'integer'],
		'archivo' => ['required'],
		'titulo' => ['required', 'min:3', 'max:250'],
		'descripcion' => ['required', 'min:3', 'max:250'],
		'activo' => ['required']
	];
}
