<?php

namespace App\Modules\Audio\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'Audio';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Audio/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Audio/Assets/css',
	];
}
