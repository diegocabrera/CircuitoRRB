<?php

namespace App\Modules\Informativo\Http\Controllers;

//Controlador Padre
use App\Modules\Informativo\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Informativo\Http\Requests\DebateRequest;

//Modelos
use App\Modules\Informativo\Models\Debate;

class DebateController extends Controller
{
    protected $titulo = 'Debate';

    public $js = [
        'Debate'
    ];

    public $css = [
        'Debate'
    ];

    public $librerias = [
        'alphanum',
        'maskedinput',
        'datatables',
        'ckeditor',
        'jquery-ui',
        'jquery-ui-timepicker',
        'jcrop',
        'file-upload'
    ];

    public function index()
    {
        return $this->view('informativo::Debate', [
            'Debate' => new Debate()
        ]);
    }

    public function nuevo()
    {
        $Debate = new Debate();
        return $this->view('informativo::Debate', [
            'layouts' => 'base::layouts.popup',
            'Debate' => $Debate
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Debate = Debate::find($id);
        return $this->view('informativo::Debate', [
            'layouts' => 'base::layouts.popup',
            'Debate' => $Debate
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Debate = Debate::withTrashed()->find($id);
        } else {
            $Debate = Debate::find($id);
        }

        if ($Debate) {
            return array_merge($Debate->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function data($request){
        if ($this->puedePublicar() && $request['published_at'] != '') {
            $data=$request;
        }else {
            $data=$request->except(['published_at']);
        }
        $data['contenido']= strip_tags($data ['contenido_html']);
        return $data;
    }

    public function puedePublicar(){
        return strtolower(auth()->user()->super) === 's' || $this->permisologia('publicar');
    }

    public function guardar(DebateRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $data = $this->data($request->all());
            $Debate = $id == 0 ? new Debate() : Debate::find($id);

            $Debate->fill($data);
            $Debate->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Debate->id,
            'texto' => $Debate->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Debate::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Debate::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Debate::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Debate::select([
            'id', 'titulo', 'slug', 'resumen', 'published_at', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
