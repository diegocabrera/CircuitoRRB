<?php

namespace App\Modules\Informativo\Http\Requests;

use App\Http\Requests\Request;

class EfemeridesImgRequest extends Request {
    protected $reglasArr = [
		'efemerides_id' => ['required', 'integer'],
		'archivo' => ['required', 'min:3', 'max:200'], 
		'tamano' => ['required', 'min:3', 'max:12']
	];
}
