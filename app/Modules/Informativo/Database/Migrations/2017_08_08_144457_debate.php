<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Debate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debate', function (Blueprint $table) {
          $table->increments('id');
          $table->string('titulo');
          $table->string('slug');
          $table->timestamp('published_at')->nullable();
          $table->text('contenido');
          $table->text('contenido_html');
          $table->text('resumen');
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('debate');
    }
}
