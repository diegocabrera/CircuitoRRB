<?php

namespace App\Modules\Informativo\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Base\Models\Usuario;
use App\Modules\Informativo\Models\EfemeridesImg;
use Carbon\Carbon;



class Efemerides extends modelo
{
    protected $table = 'efemerides';
    protected $fillable = ["titulo", 'resumen', 'url','slug','published_at'];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }

    public function imagenes(){
        // belongsTo = "pertenece a" | hace relacion desde el detalle hasta el maestro
        return $this->hasMany('App\Modules\Informativo\Models\EfemeridesImg');
    }


}
