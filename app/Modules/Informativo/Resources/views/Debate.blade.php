@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Debate']])

    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Debate.',
        'columnas' => [
            'Titulo' => '25',
            'Slug' => '25',
            'Resumen' => '35',
            'Fecha' => '15'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {{-- {!! $Debate->generate() !!} --}}
            <div class="form-group col-md-4">
                <label for="titulo" class="requerido">Titulo</label>
                <input class="form-control" required="required" id="titulo" name="titulo" type="text" placeholder="Titulo del Debate">
            </div>
            <div class="form-group col-md-4">
                <label for="slug" class="requerido">Slug</label>
                <input class="form-control" placeholder="Slug del debate" required="required" id="slug" name="slug" type="text" value="">
            </div>

            @if ($controller->puedepublicar())
            {{ Form::bsText('published_at', '', [
                'label' => 'Fecha',
                'placeholder' => 'Fecha de Publicación',
                'class_cont' => 'col-md-2'
            ]) }}
            @endif
            <div class="col-md-12"></div>
            <div class="form-group col-xs-12">
                <label for="contenido_html">Contenido </label>
                <input id="contenido_html" name="contenido_html" type="hidden" />
                <textarea  placeholder="Contenido de la noticia" id="contenido" class="form-control" required="required"></textarea>
            </div>
            <div class="form-group col-xs-12">
                <label for="resumen">Resumen </label>
                <textarea id="resumen" name="resumen" class="form-control" placeholder="Resumen de la Noticia" required="required"></textarea>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
