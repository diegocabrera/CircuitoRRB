<?php

namespace App\Modules\Multimedia\Http\Controllers;

//Controlador Padre
use App\Modules\Multimedia\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
/* use App\Modules\Multimedia\Http\Requests\VideoRequest; */

//Modelos
use App\Modules\Multimedia\Models\Video;

class VideoController extends Controller
{
    protected $titulo = 'Video';

    public $js = [
        'Video'
    ];
    
    public $css = [
        'Video'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('multimedia::Video', [
            'Video' => new Video()
        ]);
    }

    public function nuevo()
    {
        $Video = new Video();
        return $this->view('multimedia::Video', [
            'layouts' => 'base::layouts.popup',
            'Video' => $Video
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Video = Video::find($id);
        return $this->view('multimedia::Video', [
            'layouts' => 'base::layouts.popup',
            'Video' => $Video
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Video = Video::withTrashed()->find($id);
        } else {
            $Video = Video::find($id);
        }

        if ($Video) {
            return array_merge($Video->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(Request $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Video = $id == 0 ? new Video() : Video::find($id);

            $Video->fill($request->all());
            $Video->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Video->id,
            'texto' => $Video->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Video::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Video::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Video::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Video::select([
            'id', 'titulo', 'url', 'descripcion', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}