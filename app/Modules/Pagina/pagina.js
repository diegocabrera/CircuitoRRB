
//BANNER//
var sliderInterval, sliders, slider_i;
$(function(){
	$("#carousel").owlCarousel({
		animatein : 'fadeIn',
		animateOut: 'fadeOut',
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		singleItem : true,
		slideSpeed : 100,
		items:1,
		responsiveRefreshRate : 200,
		responsiveClass:true,
		dots: false,
		nav: false,
		lazyLoad:true,
    	loop:true,
		navText: [],
		onChanged: function(ev){
			console.log(ev);
		}
	});
	$("#carousel-programas").owlCarousel({
		animatein : 'fadeIn',
		animateOut: 'fadeOut',
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		singleItem : true,
		slideSpeed : 100,
		items:1,
		responsiveRefreshRate : 200,
		responsiveClass:true,
		dots: false,
		nav: true,
		lazyLoad:true,
    	loop:true,
		navText: ["<div class='btn-direccion'><i class='fa fa-chevron-left'></i></div>","<div class='btn-direccion'><i class='fa fa-chevron-right'></i></div>"],
		onChanged: function(ev){
			console.log(ev);
		}
	});
});



//-----------------VIDEOTECA------------------//
