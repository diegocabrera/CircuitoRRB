@extends('base::layouts.default')
@section('content')
@php
	function contador()
	{
		$archivo = "contador.txt"; //el archivo que contiene en numero

		$f = fopen($archivo, "r"); //abrimos el archivo en modo de lectura
		if($f)
		{
			$contador = '';
			$contador = fread($f, filesize($archivo)); //leemos el archivo
			//$contador = $contador + 1; //sumamos +1 al contador
			fclose($f);
		}
		return $contador;
	}
@endphp


	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-sm-4 col-xs-12  ">
			<div class="dashboard-stat yellow card">
				<div class="visual">
					<i class="fa fa-folder-open"></i>
				</div>
				<div class="details">
					<div class="number"> {!! contador(); !!} </div>
					<div class="desc"> Visitas </div>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
		<a href="{{url('/backend/noticias')}}" style="text-decoration:none">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="dashboard-stat card">
					<div class="visual">
						<i class="fa fa-folder"></i>
					</div>
					<div class="details">
						<h2>Noticias</h2>
					</div>
				</div>
			</div>
		</a>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="dashboard-stat blue card">
				<div class="visual">
					<i class="fa fa-gear fa-spin"></i>
				</div>
				<div class="details">
					<div class="number"> 0 </div>
					<div class="desc"> Casos en Proceso </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="dashboard-stat green card">
				<div class="visual">
					<i class="fa fa-thumbs-o-up"></i>
				</div>
				<div class="details">
					<div class="number"> 0 </div>
					<div class="desc"> Casos Resueltos </div>
				</div>
			</div>
		</div>
	</div>


@endsection
