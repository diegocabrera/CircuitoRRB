<!-- header -->
<style>
@import url('https://fonts.googleapis.com/css?family=Calligraffitti|Montez');
</style>
<div class="" id="nav" >
    <div class="header">
        <div class="" >
            <nav class="[ navbar navbar-fixed-top ][ navbar-bootsnipp animate ]" role="navigation" style="background-color:rgba(0,0,0,.75); color:black !important;">
                <div class="[ container ]">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="[ navbar-header ]">
                        <button type="button" class="[ navbar-toggle ]" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="[ sr-only ]">Toggle navigation</span>
                            <span class="[ icon-bar ]"></span>
                            <span class="[ icon-bar ]"></span>
                            <span class="[ icon-bar ]"></span>
                        </button>
                        <div class="[ animbrand ]">
                            <a class="[ animate ] col-md-2 col-sm-1 col-xs-1 col-lg-4 no-float img" href="{{url('/')}}">

                                <img src="{{url('public/img/logos/logo1.png')}}" alt="Logo" style="">

                            </a>
                        </div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="[ collapse navbar-collapse ]" id="bs-example-navbar-collapse-1">
                        <ul class="[ nav navbar-nav navbar-right ]">
                        {{-- <li class="[ visible-xs ]">
                                <form action="http://bootsnipp.com/search" method="GET" role="search">
                                    <div class="[ input-group ]">
                                        <input type="text" class="[ form-control ]" name="q" placeholder="Search for snippets">
                                        <span class="[ input-group-btn ]">
                                            <button class="[ btn btn-primary ]" type="submit"><span class="[ glyphicon glyphicon-search ]"></span></button>
                                            <button class="[ btn btn-danger ]" type="reset"><span class="[ glyphicon glyphicon-remove ]"></span></button>
                                        </span>
                                    </div>
                                </form>
                            </li>--}}
            <li><a href="{{url('/')}}" class="[ animate ] ">Inicio</a></li>
            <li>
                <a href="#" class="[ dropdown-toggle ][ animate ]" data-toggle="dropdown">Multimedia <span class="[ caret ]"></span></a>
                <ul class="[ dropdown-menu ]" role="menu">
                    <li><a href="{{url('/#videos')}}" class="[ animate ]"><span class="fa fa-video-camera">   Video</span></a></li>
                    <li><a href="{{url('/audio')}}" class="[ animate ]"><span class="fa fa-volume-up ">   Audios </span></a></li>
                    <li><a href="{{url('/#galeria')}}" class="[ animate ]"><span class="fa fa-picture-o">   Galeria de Fotos </span></a></li>
                </ul>
            </li>
            <li class="[ dropdown ]">
                <a href="#" class="[ dropdown-toggle ][ animate ]" data-toggle="dropdown">Informativo <span class="[ caret ]"></span></a>
                <ul class="[ dropdown-menu ]" role="menu">
                    <li><a href="{{url('/noticias')}}" class="[ animate ]"><span class="fa fa-tags"> Noticias </span></a></li>
                    <li><a href="{{url('/#efemerides')}}" class="[ animate ]"><span class="fa fa-calendar"> Efemerides </span></a></li>
                    <li><a href="{{url('/#eventos')}}" class="[ animate ]"> <span class="fa fa-television"> Eventos </span></a></li>
                </ul>
            </li>
            <li><a class="animate" href="{{url('/#programacion')}}">Programación</a></li>
            <li><a class="animate" href="{{url('/enlinea')}}">En Linea</a></li>
            <li><a class="animate" href="{{url('/backend')}}">Administración</a></li>

        </ul>
    </div>
</div>
</nav>
</div>
</div>
</div>

<!--//header-w3l-->
