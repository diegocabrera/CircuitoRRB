<!-- Contador de Visitas -->
 @push('php')
    <?php
        function contador()
        {
            $archivo = "contador.txt"; //el archivo que contiene en numero
            $f = fopen($archivo, "r"); //abrimos el archivo en modo de lectura
            if($f)
            {
                $contador = '';
                $contador = fread($f, filesize($archivo)); //leemos el archivo
                $contador = $contador + 1; //sumamos +1 al contador
                fclose($f);
            }
            $f = fopen($archivo, "w+");
            if($f)
            {
                fwrite($f, $contador);
                fclose($f);
            }
            return $contador;
        }
    ?>
@endpush
<!-- Reproductor -->
<div class="row" style=" height: 0px;">
    <div class="">
        <div class="col-md-1 col-sm-3 col-xs-3 col-lg-1 repro">
            <div class="repro-buttom">
                <a id="myPlayButton" href="#">
                    <h5  class="text-darken-3"> <i class="fa fa-play"></i>&nbsp;</h5>
                </a>
                <a id="myStopButton" href="#">
                    <h5  class="text-darken-3"> <i class="fa fa-stop"></i>&nbsp;</h5>
                </a>
            </div>
            <div class="repo-titulo">
                <h5 style="color:white">¡EN LINEA!</h5>
            </div>
        </div>
        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <canvas id="canvas"></canvas>
    </div>
</div>

<!--footer -->
<div class="row">
    <div class="col-md-1 col-sm-3 col-xs-3 col-lg-1 cont">
        <h6 class="cont-titulo"> ¡Visitas totales!</h6>
        <p class="cont-text">{!!contador();!!}</p>
    </div>
</div>

<div id="copyright" style="background-color: #000;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<img src="http://e-bolivar.gob.ve/public/img/Modules/Pagina/logo.png" class="img-responsive" style="margin: auto;">
			</div>
			<div class="col-xs-12 text-center">
				<div class="row ">
					<div class="col-xs-12 sociales">
						<span>
							<a href="https://www.facebook.com/gobiernodebolivar/" target="_blank" class="redes"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/RRB1011FM" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/prensagobol/" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/channel/UCbb9IJF38A3cxZKds_PDayQ" target="_blank"><i class="fa fa-youtube-play"></i></a>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-offset-2 col-md-8 col-sm-12" style="text-align: center;">
				<br><br>
				Diseñado y desarrollado por la <span style="color:#FEC72E">Dirección de Informática y Sistemas</span> Todos los derechos reservados
			</div>
			<div class="col-md-2 col-sm-12 text-center">
				<!-- LOGO DE GESTION -->
			</div>
		</div>
	</div>
</div>
