<div id="programacion">
	<div class="row">
		<div class="titulo">
			<h2 style="color:white; margin-top:1.5em; margin-bottom:1em;">Programas</h2>
		</div>
		<div class="row col-md-12">
			<div id="carousel-programas" class="owl-carousel owl-theme">
				<?php $num = 0;
				?>
				@foreach ($programaciones as $key => $programa)
					<?php
						if($num == 0 ){
							echo '<div class="item">';
						}else{
							echo '<div class="item">';
						}
						$num ++


					?>
						<div class="">
							<h4 class="modal-title contenedor-text">{{ $controller->dias[$key]}}</h4>
							<div class="modal-body">
								@foreach ($programa as $key => $value)
								<div class="col-md-3">
									<div class="contenedor" sytle="margin-right:30px; margin-left:30px;">
										<div class="ih-item square effect13 left_to_right col-md-3" sytle="margin-right:30px; margin-left:30px;">
											<a href="#programas">
											<div class="img" >
												<img style="" src="{{ url('public/img/logos-programas/'.$value['url']) }}" alt="img">
											</div>
											<div class="info">
												<h3>{{$value['titulo']}}</h3>
												<p>{{ $value['locutor'] }}</p>
												<p style="font: bold;">Inicio: {{ $value['hora_ini'] }} | Fin: {{ $value['hora_fin'] }} </p>
											</div>
											</a>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>

	</div>
</div>
