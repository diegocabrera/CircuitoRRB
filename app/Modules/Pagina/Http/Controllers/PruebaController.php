<?php

namespace App\Modules\Pagina\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Pagina\Http\Controllers;

class PruebaController extends Controller
{
    public $titulo = 'Prueba SoundCloud';
    public $css = [
        'style',
        'index',
        'Enlinea',
        'ihover.min.css',
        'mjes',
        'gdlr'
    ];
    public $js = [
        'pagina',
        'move-top',
        'jquery.jplayer.min',
        'circle.player.js',
        'player',
        'numscroller-1.0'
    ];
    public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons',
		'jquery-easing',
		'animate', 'bootstrap', 'bootbox',
		//'jquery-cookie'
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify', 'owl-carousel', 'wow', 'modernizr'
	];
    public $librerias = [
        'jquery-ui',
        'bootstrap',
        'jquery-slimscroll',
        'jquerybui',
        'scroll-top',
        'bootstrap-switch',
        'ziehharmonika'
    ];
    public function index()
    {
        return $this->view('pagina::PruebaSound');
    }

}
