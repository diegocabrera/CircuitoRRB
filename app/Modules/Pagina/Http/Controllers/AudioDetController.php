<?php

namespace App\Modules\Pagina\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Modules\Pagina\Http\Controllers;
use Carbon\Carbon;
use DB;
//MODELOS//

use App\Modules\Audio\Models\Audio;

class AudioDetController extends Controller
{
    public $titulo = 'Circuito RRB';
    public $css = [
        'style',
        'index',
        'noticias',
        'ihover.min',
        'mjes',
        'gdlr'
    ];
    public $js = [
        'noticias',
        'move-top',
        'jquery.jplayer.min',
        'circle.player.js',
        'player',
        'numscroller-1.0'
    ];
    public $dias = [
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado",
        "Domingo"
    ];
    public $meses = [
        1 => "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ];
    public $libreriasIniciales = [
		'OpenSans', 'font-awesome', 'simple-line-icons',
		'jquery-easing',
		'animate', 'bootstrap', 'bootbox',
		//'jquery-cookie'
		'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify', 'owl-carousel', 'wow', 'modernizr'
	];
    public $librerias = [
        'jquery-ui',
        'bootstrap',
        'jquery-slimscroll',
        'jquerybui',
        'scroll-top',
        'bootstrap-switch',
        'ziehharmonika'
    ];


    public function index($slug)
    {
        $audios = Audio::select(
            'id',
            'titulo',
            'slug',
            'archivo',
            'descripcion'
        )
        ->where('activo', 'like', '%si%')
        ->where('slug', '=', $slug)
        ->get();
        return $this->view('pagina::audiodet', [
            'audios'     =>      $audios
        ]);
    }

}
