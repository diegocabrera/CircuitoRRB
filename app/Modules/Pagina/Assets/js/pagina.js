
//BANNER//
var sliderInterval, sliders, slider_i;
$(function(){
	$("#carousel").owlCarousel({
		animateOut: 'fadeOut',
		autoplay:true,
		autoplayTimeout:8000,
		autoplayHoverPause:true,
		singleItem : true,
		slideSpeed : 400,
		items:1,
		responsiveRefreshRate : 200,
		responsiveClass:true,
		dots: false,
		nav: false,
		lazyLoad:true,
    	loop:true,
		navText: ["<div class='btn-direccion'><i class='fa fa-chevron-left'></i></div>","<div class='btn-direccion'><i class='fa fa-chevron-right'></i></div>"],
		onChanged: function(ev){
			console.log(ev);
		}
	});
});
	$("#carousel-programas").owlCarousel({
		animateOut: 'fadeOutDown',
    	animateIn: 'fadeInDown',
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		singleItem : true,
		slideSpeed : 100,
		items:1,
		responsiveRefreshRate : 200,
		responsiveClass:true,
		dots: true,
		lazyLoad:true,
    	loop:true,
		onChanged: function(ev){
			console.log(ev);
		}
	});



//-----------------VIDEOTECA------------------//
