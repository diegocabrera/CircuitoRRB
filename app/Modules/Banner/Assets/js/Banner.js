var aplicacion, $form, tabla, $archivos = {};
$(function() {
	aplicacion = new app('formulario', {
		'antes' : function(accion){
			$("#archivo").val(jsonToString($archivos));
		},
		'limpiar' : function(){
			tabla.fnDraw();
			$archivos = {};

			$("table tbody tr", "#fileupload").remove();
		},
		'buscar' : function(r){
			$("table tbody", "#fileupload").html(tmpl("template-download", r));
			$("table tbody .fade", "#fileupload").addClass('in');

			var archivos = r.files;
			$archivos = {};
			for(var i in archivos){
				$archivos[archivos[i].id] = archivos[i].data;
			}
		},
		'guardar' : function(){},
		'eliminar' : function(){}
	});

	$form = aplicacion.form;

	tabla = $('#tabla')
	.on('click', 'tbody tr', function(){
		aplicacion.buscar(this.id);
	})
	.dataTable({
		ajax: $url + 'datatable',
		columns: [
			{"data":"categoria","name":"categorias.descripcion"},
			{"data":"archivo","name":"banner.archivo"},
			{"data":"activo","name":"banner.activo"}
		]
	});

	$('#fileupload').fileupload({
		url: $url + 'subir',
		success: function(r){
			/*En caso de que falle la validación remueve el <tr> agregado y
			emite el mensaje de la validacion*/
			if(r.s=='n'){
				aviso(r.msj);
				$("table tbody tr", "#fileupload").remove();
				return false;
			}
		},
		disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
		maxFileSize: 999000,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i
	}).bind('fileuploaddone', function (e, data) {

		if(data.result.files){
			var archivo = data.result.files[0];
			$archivos[archivo.id] = archivo.data;
		}
	});

	$('#fileupload').on('click', '.btn-danger', function(evn){
		evn.preventDefault();
		delete $archivos[$(this).parents('tr').data('id')];
	});

});


function dataImagen(cordenadas){
	cordenadasImagen = cordenadas;
}

function stringToJson(str){
	return $.parseJSON(str);
}

function jsonToString(json){
	return JSON.stringify(json);
}
