<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class organizacion extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create('organizacion', function (Blueprint $table) {
			$table->increments('id');
			$table->string('mision', 2500);
			$table->string('vision', 2500);
			$table->string('objetivo', 2500);
			$table->timestamps();
			$table->softDeletes();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::dropIfExists('organizacion');
     }
}
